#include "Student.h"

Student::Student(string name, int RA, int semester): name(name), RA(RA), semester(semester) {
	vector<Subject *> h;
	for (int k = 0; k < semester; k++)
		semesters.push_back(h);
}

Student::~Student() {
}

void Student::addSubject(Subject *s) {
	for (int k = 0; k < this->semesters[semester - 1].size(); ++k)
		if (this->semesters[semester - 1][k] == s) {
			cout << "Matéria já existente!" << endl;
			return;
		}
	this->semesters[semester - 1].push_back(s);
	return;
}

Subject *Student::getSubject(int semester, int subject) {
	return this->semesters[semester - 1][subject - 1];
}