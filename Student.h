#ifndef STUDENT_H
#define STUDENT_H

#include "Options.h"
#include "Subject.h"

using namespace std;
using namespace opt;

class Student {	// [PT] Estudante
public:
	// Constructor and Destructor: <-EN/PT-> Construtor e Destrutor:
    Student(string name, int RA, int semester);
    virtual ~Student();
    // Subject adder: / Adicionador de matérias:
    void addSubject(Subject *s);
    // Subject remover / Removedor de matérias:
    void removeSubject(string n_a); // Can recieve Name or Acronym. / Pode receber Nome ou Sigla.
    // Gets and Sets: / Gets e Sets:
    // Subject: / Matéria:
    Subject *getSubject(int semester, int subject);
    // Name: / Nome:
    string getName();
    void setName(string name);
    // RA: / RA:
    int getRA();
    void setRA(int RA);
    vector<vector<Subject *> > semesters; // PUBLIC FOR TESTING PURPOSES! / PÚBLICO PARA MOTIVOS DE TESTE!
private:
    string name;
    int RA, semester; // Academic Registry, current semester. / Registro Acadêmico, semestre atual.
    //vector<vector<Subject *> > semesters; // All subjects from all semesters. / Todas as matérias de todos os semestres.
};

#endif