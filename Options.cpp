#include "Options.h"

#include <iostream>

namespace opt {
	int lang = 0;
	void const setLanguage() {
		int l = -1;
		std::cout << "[" << PT << "] Português / [" << EN << "] English" << std::endl;
		while (l != PT && l != EN)
			std::cin >> l;
		lang = l;
	};
}