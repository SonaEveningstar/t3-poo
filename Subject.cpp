#include "Subject.h"
#include "Options.h"

Subject::Subject(string name, string acro, Professor* professor, int profile, int aTests, int aWorks): name(name), acro(acro), professor(professor), profile(profile), aTests(aTests), aWorks(aWorks) {
	for (int k = 0; k < this->aTests; ++k)
		this->tGrades.push_back(-1.0);
	for (int k = 0; k < this->aWorks; ++k)
		this->wGrades.push_back(-1.0);
	this->schedule.push_back(-1.0);
}

Subject::~Subject() {
}

string Subject::getName() {
	return this->name;
}

void Subject::setName(string name) {
	this->name = name;
}

string Subject::getAcro() {
	return this->acro;
}

void Subject::setAcro(string acro) {
	this->acro = acro;
}

double readGrade() {
	double aux = -1;
	while (aux < 0 || aux > 10)
		cin >> aux;
	return aux;
}

void Subject::setGrades() {
	string texts[] = {"Que nota você quer mudar?", "Which grade do you wanna change?", "Sem nota disponível.", "No grade available.", "Qual é o novo valor?", "What is the new value?"};
	int aux = 1, c = -1, textsAux = 0;
	double n;
	cout << texts[textsAux * aLangs + lang] << endl;
	++textsAux;
	for (int k = 0; k < tGrades.size(); ++k)
		if (this->tGrades[k] >= 0)
			cout << "[" << k << "] " << "P" << k+1 << ": " << this->tGrades[k] << endl;
		else
			cout << "[" << k << "] " << "P" << k+1 << ": " << texts[textsAux * aLangs + lang] << endl;
	for (int k = 0; k < wGrades.size(); ++k)
		if (this->wGrades[k] >= 0)
			cout << "[" << k + this->tGrades.size() << "] " << "T" << k+1 << ": " << this->wGrades[k] << endl;
		else
			cout << "[" << k + this->tGrades.size() << "] " << "T" << k+1 << ": " << texts[textsAux * aLangs + lang] << endl;
	++textsAux;
	while (c > (this->tGrades.size() + this->wGrades.size() - 1) || c < 0)
		cin >> c;
	if (c >= tGrades.size()) {
		c -= tGrades.size();
		--aux;
	}
	cout << texts[textsAux * aLangs + lang] << endl;
	++textsAux;
	n = readGrade();
	if (aux)
		this->tGrades[c] = n;
	else
		this->wGrades[c] = n;
	return;
}

Professor* Subject::getProfessor() {
	return this->professor;
}

void Subject::setProfessor(Professor* professor) {
	this->professor = professor;
}

void Subject::printGrades() {
	string texts[] = {"Ainda não constam notas!", "There are no grades yet!", "P", "T", "T", "W"};
	int textsAux = 0;
	if (this->tGrades[0] < 0 && this->wGrades[0] < 0) {
		cout << texts[textsAux * aLangs + lang] << endl;
		return;
	}
	++textsAux;
	for (int k = 0; k < this->tGrades.size(); ++k)
		if (this->tGrades[k] >= 0)
			cout << texts[textsAux * aLangs + lang] << k+1 << ": " << this->tGrades[k] << endl;
		else
			break;
	++textsAux;
	for (int k = 0; k < this->wGrades.size(); ++k)
		if (this->wGrades[k] >= 0)
			cout << texts[textsAux * aLangs + lang] << k+1 << ": " << this->wGrades[k] << endl;
		else
			break;
	++textsAux;
}

void Subject::printSchedule() {
	string texts[] = {"Os horários ainda não foram definidos!", "The schedule wasn't defined yet!"};
	if (this->schedule[0] < 0) {
		cout << texts[lang] << endl;
		return;
	}
	string weekDays[] = {"Segunda-feira", "Monday", "Terça-feira", "Tuesday", "Quarta-feira", "Wednesday", "Quinta-feira", "Thursday", "Sexta-feira", "Friday"};
	int timetable[] = {8, 10, 12, 14, 16, 19, 21};
	int bC, aC; 
	for (int k = 0; k < schedule.size(); ++k) {
		bC = floor(this->schedule[k]);
		aC = (this->schedule[k] - bC) * 10;
		cout << weekDays[bC * aLangs + lang] << " " << timetable[aC] << ":00" << endl; 
	}
	return;
}

void Subject::printSubject() {
	string texts[] = {"Professor: ", "Professor: "};
	int textsAux = 0;
	cout << "[" << this->getAcro() << "] " << this->getName() << endl;
	cout << texts[textsAux * aLangs + lang] << this->getProfessor()->getName() << endl;
	++textsAux;
	this->printSchedule();
	this->printGrades();
}