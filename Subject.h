#ifndef SUBJECT_H
#define SUBJECT_H

#include <iostream>
#include <cmath>
#include <vector>
#include "Professor.h"

using namespace opt;
using namespace std;

class Subject { // [PT] Matéria
public:
	// Constructor and Destructor: <-EN/PT-> Construtor e Destrutor:
	Subject(string name, string acro, Professor* professor, int profile, int aTests, int aWorks);
	virtual ~Subject();
	// Gets and Sets: / Gets e Sets:
	// Name: / Nome:
	string getName();
	void setName(string name);
	 // Acronym: / Sigla:
	string getAcro();
	void setAcro(string acro);
	// Grades: / Notas:
	double getGrades();
	void setGrades();
	// Professor: / Professor:
	Professor* getProfessor();
	void setProfessor(Professor* professor);
	// Prints: / Impressões:
	void printGrades();
	void printSchedule();
	void printSubject();
	vector<float> schedule; // PUBLIC FOR TESTING PURPOSES! / PÚBLICO PARA MOTIVOS DE TESTE!
private:
	vector<double> tGrades, wGrades; // testGrades and workGrades. / provaNotas e trabalhoNotas.
	vector<float> /*schedule,*/ gAux; // They're both auxiliar vectors. / Ambos são vectors auxiliares.
	Professor* professor;
	string name, acro; // Acro stands for Acronym. / Acro vem de Sigla.
	int aTests, aWorks, skips, profile, lState; // amountTests, amountWorks, skips, profile, lockState. / quantidadeProvas, quantidadeTrabalhos, faltas, perfil, trancarEstado
};

#endif