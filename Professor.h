#ifndef PROFESSOR_H
#define PROFESSOR_H

#include <iostream>
#include "Options.h"

using namespace std;
using namespace opt;

class Professor {
public:
	Professor(string name);
	virtual ~Professor();
	string getName();
private:
	string name;
};

#endif