#ifndef COLLEGE_H
#define COLLEGE_H

#include <iostream>
#include <vector>
#include "Options.h"
#include "Student.h"
#include "Subject.h"

using namespace std;
using namespace opt;

class College {
public:
	College();
	virtual ~College();
private:
	vector<Student *> students;
	vector<Subject *> subjects;
};

#endif