#include <iostream>
#include "Options.h"
#include "Student.h"

using namespace std;
using namespace opt;

int main() {
	setLanguage();
	Student *a = new Student("Sona", 769802, 2);
	Professor *d = new Professor("Delano");
	Subject *s = new Subject("Programacao Orientada a Objetos", "POO", d, 2, 3, 3);
	a->addSubject(s);
	cout << a->getSubject(2, 1)->getAcro() << endl;
	setLanguage();
	(a->semesters[1][0])->setGrades();
	(a->semesters[1][0])->printSubject();
	setLanguage();
	(a->semesters[1][0])->setGrades();
	(a->semesters[1][0])->printSubject();
	return 0;
}